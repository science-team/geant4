#!/bin/sh

# copy basic examples to temporary folder
cp -R /usr/share/doc/geant4/examples/basic/* $AUTOPKGTEST_TMP

# create and switch to build folder
mkdir $AUTOPKGTEST_TMP/builddir
cd $AUTOPKGTEST_TMP/builddir

# setup cmake
cmake .. -Wno-dev

# build basic examples
make

# run B1 example
#cd ./B1
#g4env ./exampleB1 run1.mac

# return
return $?
